$("#begin_left_to_right_animation").on("click", function(){
	KEY.beginLeftToRightAnimation();
});

$("#begin_right_to_left_animation").on("click", function(){
	KEY.beginRightToLeftAnimation();
});

$("#animate_circles").on("click", function(){
	KEY.animateCircles();
});



var KEY = (function(){
	function _release_public(){
		return {
			init: init,
			beginLeftToRightAnimation: beginLeftToRightAnimation,
			beginRightToLeftAnimation: beginRightToLeftAnimation,
			animateCircles: animateCircles
		}
	};
	
	/*---------------private-------------------*/
	
	var _last_top_circle = 0,
		_last_left_circle = 0,
		_last_touch = "l_to_r";
	
	/*---------------public--------------------*/
	
	/**
	 * Begin Left To Right Animation
	 * 
	 * Created a new circle and moves it left to right
	 */
	function beginLeftToRightAnimation(){	
		console.log("create a circle and move it left to right");
		var html = '';
	
		if(_last_touch === "l_to_r"){
			_last_top_circle += 20;
			_last_left_circle += 10;
		}else{
			console.log("KEY::beginLeftToRightAnimation: resetting start position and clearing article");
			$(".my_circle", "#left_to_right_article").remove();
			_last_top_circle = 0;
			_last_left_circle = 0;
		}
	
		html += ''															+
		'<span class="my_circle"></span>'									+
		'';
		
		$(html).appendTo($("#left_to_right_article")).css({
			top: _last_top_circle,
			left: _last_left_circle
		});
		
		_last_touch = "l_to_r";
	};
	
	/**
	 * Begin Right To Left Animation
	 * 
	 * Created a new circle and moves it right to left
	 */
 	function beginRightToLeftAnimation(){
 		console.log("create a circle and move it right to left");
 		var html = '';
	
		if(_last_touch === "r_to_l"){
			_last_top_circle += 20;
			_last_left_circle += 20;
		}else{
			console.log("KEY::beginRightToLeftAnimation: resetting start position and clearing article");
			$(".my_circle", "#right_to_left_article").remove();
			_last_top_circle = 0;
			_last_left_circle = 0;
		}
	
 		html += ''															+
 		'<span class="my_circle"></span>'									+
 		'';
		
 		$(html).appendTo($("#right_to_left_article")).css({
 			top: _last_top_circle,
 			left: _last_left_circle
 		});
		
		_last_touch = "r_to_l";
 	};
	/**
	 * Animate Circles
	 * 
	 * adjusts a class for keyframes to kick in
	 */
	function animateCircles(){
		console.log("animate now by changing class on all circles");	
		$(".my_circle").addClass("move_me");
	};
	
	
	function init(){
		console.log("KEY::init: Good to go!");	
	};
		
	return _release_public();
})();



	


